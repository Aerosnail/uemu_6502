#pragma once

#include <stdint.h>
#include "architecture.h"

enum addr_mode {
	ADDR_INVALID = 0,
	ADDR_ACC,
	ADDR_IMM,
	ADDR_REL,
	ADDR_ZP,
	ADDR_ZP_X,
	ADDR_ZP_Y,
	ADDR_IND_ZP,
	ADDR_IND_ZP_X,
	ADDR_IND_ZP_Y,
	ADDR_ABS,
	ADDR_ABS_X,
	ADDR_ABS_Y,
	ADDR_IND,
};

uint16_t mem_resolve(struct cpu *cpu, enum addr_mode addr_mode);
uint16_t mem_resolve_cond(struct cpu *cpu, enum addr_mode addr_mode, int cond);
