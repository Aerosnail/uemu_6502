#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <command.h>
#include <log/log.h>

#include "gdb/gdb.h"
#include "architecture.h"
#include "commands.h"
#include "cpu.h"
#include "module.h"

#ifndef LEN
#define LEN(x) (sizeof(x)/sizeof(*(x)))
#endif

typedef struct {
	const char *cmd;
	const char *help;
	int (*fcn)(int, const char*[], void*);
} command_t;

static int dumpstate(int argc, const char *argv[], void *ctx);
static int reset(int argc, const char *argv[], void *ctx);
static int step(int argc, const char *argv[], void *ctx);
static int halt(int argc, const char *argv[], void *ctx);
static int run(int argc, const char *argv[], void *ctx);
static int breakpoint(int argc, const char *argv[], void *ctx);
static int until(int argc, const char *argv[], void *ctx);
static int gdb(int argc, const char *argv[], void *ctx);

static const command_t cmds[] = {
	{
		.cmd = "dumpstate",
		.help = "Show the internal state of the processor",
		.fcn = dumpstate,
	},
	{
		.cmd = "reset",
		.help = "Reset the processor",
		.fcn = reset,
	},
	{
		.cmd = "step",
		.help = "Step through the next instruction",
		.fcn = step,
	},
	{
		.cmd = "run",
		.help = "Start executing instructions",
		.fcn = run,
	},
	{
		.cmd = "halt",
		.help = "Halt instruction execution",
		.fcn = halt
	},
	{
		.cmd = "break",
		.help = "Add a breakpoint when PC=value",
		.fcn = breakpoint
	},
	{
		.cmd = "until",
		.help = "Execute instructions until PC=value",
		.fcn = until,
	},
	{
		.cmd = "gdb",
		.help = "Start/stop a GDB stub on the specified port",
		.fcn = gdb,
	},
};

int
register_commands(const char *name, struct cpu *ctx)
{
	int ret = 0;
	size_t i;

	for (i=0; i<LEN(cmds); i++) {
		ret |= cmd_register_module(name,
				cmds[i].cmd, cmds[i].help, cmds[i].fcn, ctx);
	}

	return ret;
}

int
unregister_commands(const char *name)
{
	return cmd_unregister_module(name, NULL);
}

static int
dumpstate(int argc, const char *argv[], void *ctx)
{
	(void)argc;
	(void)argv;
	struct cpu *self = (struct cpu*)ctx;

	printf("[%04x] A = 0x%02x X = 0x%02x Y = 0x%02x SP = 0x%02x\n",
			(unsigned)self->regs.pc,
			(unsigned)self->regs.a,
			(unsigned)self->regs.x,
			(unsigned)self->regs.y,
			(unsigned)self->regs.sp
	);

	return 0;
}

static int
reset(int argc, const char *argv[], void *ctx)
{
	(void)argc;
	(void)argv;

	struct cpu *self = (struct cpu*)ctx;
	return cpu_reset(self);
}

static int
step(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	unsigned long stepcount = 1;
	int ret = 0;

	if (argc > 1) {
		stepcount = strtoul(argv[1], NULL, 10);
	}

	if (self->tid) {
		log_error("Cannot step while CPU is running");
		return EBUSY;
	}

	for (; stepcount > 0; stepcount--) {
		ret |= cpu_step(self);

		log_debug("[%04x] A = 0x%02x X = 0x%02x Y = 0x%02x SP = 0x%02x",
		          (unsigned)self->regs.pc,
		          (unsigned)self->regs.a,
		          (unsigned)self->regs.x,
		          (unsigned)self->regs.y,
		          (unsigned)self->regs.sp
		);
	}

	return ret;
}

static int
until(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	uint32_t pc;
	int ret = 0;

	if (argc < 2) {
		return 1;
	}

	pc = strtoul(argv[1], NULL, 0);

	if (self->tid) {
		log_error("Cannot step while CPU is running");
		return 1;
	}

	while (self->regs.pc != pc) {
		ret |= cpu_step(self);
		ret |= dumpstate(0, NULL, self);
	}

	return ret;
}

static int
breakpoint(int argc, const char *argv[], void *ctx)
{
	/* TODO implement */
	return ENOSYS;
}

static int
run(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	(void)argc;
	(void)argv;

	return cpu_run(self);
}

static int
halt(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	(void)argc;
	(void)argv;

	return cpu_stop(self);
}

static int
gdb(int argc, const char *argv[], void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	uint16_t port;
	int ret;

	if (argc < 2) {
		log_warn("Usage: %s <start | stop>", argv[0]);
		return EINVAL;
	}

	if (!strcmp(argv[1], "start")) {

		if (self->gdb) {
			log_warn("GDB stub already running");
			return EBUSY;
		}

		port = (argc < 2) ? 9001 : atoi(argv[2]);
		ret = gdb_start_stub(self, port);

		if (!ret) {
			log_info("GDB stub ready on port %d", port);
		}
	} else if (!strcmp(argv[1], "stop")) {
		if (!self->gdb) {
			log_warn("GDB stub not running");
			return ECHILD;
		}

		ret = gdb_stop_stub(self);
	}

	return ret;
}
