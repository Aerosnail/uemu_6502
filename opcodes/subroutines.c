#include "subroutines.h"

int
jmp(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	cpu->regs.pc = addr;
	cpu->instr_size = 0;

	return 0;
}

int
jsr(struct cpu *cpu, enum addr_mode addr_mode)
{
	int ret;
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	const uint16_t stack = 0x100 | (uint8_t)(cpu->regs.sp - 1);
	uint16_t target;

	target = cpu->regs.pc + cpu->instr_size - 1;
	ret = mem_write(cpu->bus, stack, &target, sizeof(target));
	cpu->regs.sp -= 2;

	cpu->regs.pc = addr;
	cpu->instr_size = 0;

	return ret;
}

int
rts(struct cpu *cpu)
{
	int ret;
	const uint16_t stack = 0x100 | (uint8_t)(cpu->regs.sp + 1);
	uint16_t target;

	ret = mem_read(cpu->bus, stack, &target, sizeof(target));
	cpu->regs.sp += 2;

	cpu->regs.pc = target + 1;
	cpu->instr_size = 0;

	return ret;
}

int
rti(struct cpu *cpu)
{
	int ret;
	uint16_t stack;
	uint16_t target;
	uint8_t status;

	cpu->regs.sp++;
	stack = 0x100 | cpu->regs.sp;
	ret = mem_read(cpu->bus, stack, &status, sizeof(status));

	stack = 0x100 | (uint8_t)(cpu->regs.sp + 1);
	ret |= mem_read(cpu->bus, stack, &target, sizeof(target));
	cpu->regs.sp += 2;

	cpu->instr_size = 0;

	cpu->regs.pc = target;
	cpu->regs.sr.n = (status >> 7) & 0x1;
	cpu->regs.sr.v = (status >> 6) & 0x1;
	cpu->regs.sr.d = (status >> 3) & 0x1;
	cpu->regs.sr.i = (status >> 2) & 0x1;
	cpu->regs.sr.z = (status >> 1) & 0x1;
	cpu->regs.sr.c = status & 0x1;

	return ret;
}

