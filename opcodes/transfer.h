#pragma once

#include "architecture.h"

int tax(struct cpu *cpu);
int txa(struct cpu *cpu);
int tay(struct cpu *cpu);
int tya(struct cpu *cpu);
int tsx(struct cpu *cpu);
int txs(struct cpu *cpu);
