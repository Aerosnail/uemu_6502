#pragma once

#include "architecture.h"
#include "memops.h"

int lda(struct cpu *cpu, enum addr_mode addr_mode);
int ldx(struct cpu *cpu, enum addr_mode addr_mode);
int ldy(struct cpu *cpu, enum addr_mode addr_mode);

int sta(struct cpu *cpu, enum addr_mode addr_mode);
int stx(struct cpu *cpu, enum addr_mode addr_mode);
int sty(struct cpu *cpu, enum addr_mode addr_mode);
int stz(struct cpu *cpu, enum addr_mode addr_mode);
