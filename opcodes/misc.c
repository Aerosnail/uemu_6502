#include "misc.h"

int
brk(struct cpu *cpu)
{
	uint16_t stack;
	uint16_t pushed_pc;
	uint8_t status;
	int ret = 0;

	status = !!cpu->regs.sr.n << 7
	       | !!cpu->regs.sr.v << 6
	       | !!cpu->regs.sr.d << 3
	       | !!cpu->regs.sr.i << 2
	       | !!cpu->regs.sr.z << 1
	       | !!cpu->regs.sr.c;

	stack = 0x100 | (uint8_t)(cpu->regs.sp - 1);
	pushed_pc = cpu->regs.pc + 1;
	mem_write(cpu->bus, stack, &pushed_pc, sizeof(cpu->regs.pc));
	cpu->regs.sp -= 2;

	stack = 0x100 | cpu->regs.sp;
	mem_write(cpu->bus, stack, &status, sizeof(status));
	cpu->regs.sp--;

	ret = mem_read(cpu->bus, BRK_VECTOR_ADDR, &cpu->regs.pc, sizeof(cpu->regs.pc));

	cpu->regs.sr.b = 1;
	cpu->regs.sr.i = 1;

	return ret;
}

int
nop(struct cpu *cpu)
{
	(void)cpu;
	return 0;
}

int
wai(struct cpu *cpu)
{
	cpu->wai = true;
	return 0;
}

int
stp(struct cpu *cpu)
{
	cpu->stp = true;
	return 0;
}

