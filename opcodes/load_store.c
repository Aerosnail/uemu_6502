#include "load_store.h"

int
lda(struct cpu *cpu, enum addr_mode addr_mode)
{
	int ret;
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	ret = mem_read(cpu->bus, addr, &cpu->regs.a, sizeof(cpu->regs.a));
	cpu->regs.sr.n = cpu->regs.a >> 7;
	cpu->regs.sr.z = cpu->regs.a == 0;

	return ret;
}

int
ldx(struct cpu *cpu, enum addr_mode addr_mode)
{
	int ret;
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	ret = mem_read(cpu->bus, addr, &cpu->regs.x, sizeof(cpu->regs.x));
	cpu->regs.sr.n = cpu->regs.x >> 7;
	cpu->regs.sr.z = cpu->regs.x == 0;

	return ret;
}

int
ldy(struct cpu *cpu, enum addr_mode addr_mode)
{
	int ret;
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	ret = mem_read(cpu->bus, addr, &cpu->regs.y, sizeof(cpu->regs.y));
	cpu->regs.sr.n = cpu->regs.y >> 7;
	cpu->regs.sr.z = cpu->regs.y == 0;

	return ret;
}

int
sta(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	return mem_write(cpu->bus, addr, &cpu->regs.a, sizeof(cpu->regs.a));
}

int
stx(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	return mem_write(cpu->bus, addr, &cpu->regs.x, sizeof(cpu->regs.x));
}

int
sty(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);

	return mem_write(cpu->bus, addr, &cpu->regs.y, sizeof(cpu->regs.y));
}

int
stz(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	const uint8_t zero = 0x00;

	return mem_write(cpu->bus, addr, &zero, sizeof(zero));
}
