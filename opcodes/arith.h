#pragma once

#include "architecture.h"
#include "memops.h"

int adc(struct cpu *cpu, enum addr_mode addr_mode);
int sbc(struct cpu *cpu, enum addr_mode addr_mode);

int inc(struct cpu *cpu, enum addr_mode addr_mode);
int inca(struct cpu *cpu);
int inx(struct cpu *cpu);
int iny(struct cpu *cpu);

int dec(struct cpu *cpu, enum addr_mode addr_mode);
int deca(struct cpu *cpu);
int dex(struct cpu *cpu);
int dey(struct cpu *cpu);
