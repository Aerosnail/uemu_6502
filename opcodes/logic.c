#include "logic.h"

int
and(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = cpu->regs.a & mem;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return ret;
}

int
ora(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = cpu->regs.a | mem;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return ret;
}

int
eor(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = cpu->regs.a ^ mem;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return ret;
}

