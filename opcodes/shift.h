#pragma once

#include "architecture.h"
#include "memops.h"

int asl_acc(struct cpu *cpu);
int lsr_acc(struct cpu *cpu);
int rol_acc(struct cpu *cpu);
int ror_acc(struct cpu *cpu);

int asl(struct cpu *cpu, enum addr_mode mode);
int lsr(struct cpu *cpu, enum addr_mode mode);
int rol(struct cpu *cpu, enum addr_mode mode);
int ror(struct cpu *cpu, enum addr_mode mode);
