#include "branch.h"
#include "architecture.h"

int
bra(struct cpu *cpu)
{
	uint16_t addr = mem_resolve(cpu, ADDR_REL);
	cpu->regs.pc = addr;
	return 0;
}

int
bcc(struct cpu *cpu)
{
	const int cond = !cpu->regs.sr.c;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bcs(struct cpu *cpu)
{
	const int cond = cpu->regs.sr.c;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bne(struct cpu *cpu)
{
	const int cond = !cpu->regs.sr.z;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
beq(struct cpu *cpu)
{
	const int cond = cpu->regs.sr.z;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bpl(struct cpu *cpu)
{
	const int cond = !cpu->regs.sr.n;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bmi(struct cpu *cpu)
{
	const int cond = cpu->regs.sr.n;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bvc(struct cpu *cpu)
{
	const int cond = !cpu->regs.sr.v;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bvs(struct cpu *cpu)
{
	const int cond = cpu->regs.sr.v;
	uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bbr(struct cpu *cpu, int bit)
{
	const int cond = !(cpu->regs.a & (1 << bit));
	const uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}

int
bbs(struct cpu *cpu, int bit)
{
	const int cond = (cpu->regs.a & (1 << bit));
	const uint16_t addr = mem_resolve(cpu, ADDR_REL);

	if (cond) {
		cpu->regs.pc = addr;
	}

	return 0;
}
