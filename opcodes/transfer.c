#include "transfer.h"

int
tax(struct cpu *cpu)
{
	cpu->regs.x = cpu->regs.a;

	cpu->regs.sr.n = cpu->regs.x >> 7;
	cpu->regs.sr.z = cpu->regs.x == 0;

	return 0;
}

int
txa(struct cpu *cpu)
{
	cpu->regs.a = cpu->regs.x;

	cpu->regs.sr.n = cpu->regs.a >> 7;
	cpu->regs.sr.z = cpu->regs.a == 0;

	return 0;
}

int
tay(struct cpu *cpu)
{
	cpu->regs.y = cpu->regs.a;

	cpu->regs.sr.n = cpu->regs.y >> 7;
	cpu->regs.sr.z = cpu->regs.y == 0;

	return 0;
}

int
tya(struct cpu *cpu)
{
	cpu->regs.a = cpu->regs.y;

	cpu->regs.sr.n = cpu->regs.a >> 7;
	cpu->regs.sr.z = cpu->regs.a == 0;

	return 0;
}

int
tsx(struct cpu *cpu)
{
	cpu->regs.x = cpu->regs.sp;

	cpu->regs.sr.n = cpu->regs.x >> 7;
	cpu->regs.sr.z = cpu->regs.x == 0;

	return 0;
}

int
txs(struct cpu *cpu)
{
	cpu->regs.sp = cpu->regs.x;
	return 0;
}

