#pragma once

#include "architecture.h"
#include "memops.h"

int and(struct cpu *cpu, enum addr_mode addr_mode);
int ora(struct cpu *cpu, enum addr_mode addr_mode);
int eor(struct cpu *cpu, enum addr_mode addr_mode);
