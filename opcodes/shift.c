#include "shift.h"

int
asl_acc(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.a << 1;

	cpu->regs.sr.c = cpu->regs.a >> 7;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return 0;
}

int
lsr_acc(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.a >> 1;

	cpu->regs.sr.c = cpu->regs.a & 0x01;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return 0;
}

int
rol_acc(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.a << 1 | cpu->regs.sr.c;

	cpu->regs.sr.c = cpu->regs.a >> 7;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return 0;
}

int
ror_acc(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.a >> 1 | (uint8_t)cpu->regs.sr.c << 7;

	cpu->regs.sr.c = cpu->regs.a & 0x01;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return 0;
}


int
asl(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	int ret;
	uint8_t mem, result;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = mem << 1;
	ret |= mem_write(cpu->bus, addr, &result, sizeof(result));

	cpu->regs.sr.c = mem >> 7;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;

	return ret;
}

int
lsr(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	int ret;
	uint8_t mem, result;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = mem >> 1;
	ret |= mem_write(cpu->bus, addr, &result, sizeof(result));

	cpu->regs.sr.c = mem & 0x1;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;

	return ret;
}

int
rol(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	int ret;
	uint8_t mem, result;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = mem << 1 | cpu->regs.sr.c;
	ret |= mem_write(cpu->bus, addr, &result, sizeof(result));

	cpu->regs.sr.c = mem >> 7;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;

	return ret;
}

int
ror(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	int ret;
	uint8_t mem, result;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	result = mem >> 1 | cpu->regs.sr.c << 7;
	ret |= mem_write(cpu->bus, addr, &result, sizeof(result));

	cpu->regs.sr.c = mem & 0x1;
	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;

	return ret;
}

