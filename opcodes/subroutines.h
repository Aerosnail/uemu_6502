#pragma once

#include "architecture.h"
#include "memops.h"

int jmp(struct cpu *cpu, enum addr_mode addr_mode);
int jsr(struct cpu *cpu, enum addr_mode addr_mode);
int rts(struct cpu *cpu);
int rti(struct cpu *cpu);
