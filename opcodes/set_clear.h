#pragma once

#include "architecture.h"
#include "memops.h"

int clc(struct cpu *cpu);
int sec(struct cpu *cpu);
int cld(struct cpu *cpu);
int sed(struct cpu *cpu);
int cli(struct cpu *cpu);
int sei(struct cpu *cpu);
int clv(struct cpu *cpu);
int rmb(struct cpu *cpu, enum addr_mode addr_mode, int bit);
int smb(struct cpu *cpu, enum addr_mode addr_mode, int bit);
