#pragma once

#include "architecture.h"

int brk(struct cpu *cpu);
int nop(struct cpu *cpu);
int wai(struct cpu *cpu);
int stp(struct cpu *cpu);
