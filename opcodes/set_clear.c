#include "set_clear.h"

int
clc(struct cpu *cpu)
{
	cpu->regs.sr.c = 0;
	return 0;
}

int
sec(struct cpu *cpu)
{
	cpu->regs.sr.c = 1;
	return 0;
}

int
cld(struct cpu *cpu)
{
	cpu->regs.sr.d = 0;
	return 0;
}

int
sed(struct cpu *cpu)
{
	cpu->regs.sr.d = 1;
	return 0;
}

int
cli(struct cpu *cpu)
{
	int_set_masked(cpu->int_mgr, INT_IRQ, 0);
	cpu->regs.sr.i = 0;
	return 0;
}

int
sei(struct cpu *cpu)
{
	int_set_masked(cpu->int_mgr, INT_IRQ, 1);
	cpu->regs.sr.i = 1;
	return 0;
}

int
clv(struct cpu *cpu)
{
	cpu->regs.sr.v = 0;
	return 0;
}

int
rmb(struct cpu *cpu, enum addr_mode addr_mode, int bit)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	int ret;
	uint8_t mem;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	mem &= ~(1 << bit);
	ret |= mem_write(cpu->bus, addr, &mem, sizeof(mem));

	return ret;
}

int
smb(struct cpu *cpu, enum addr_mode addr_mode, int bit)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	int ret;
	uint8_t mem;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	mem |= (1 << bit);
	ret |= mem_write(cpu->bus, addr, &mem, sizeof(mem));

	return ret;
}

