#include "architecture.h"
#include "compare.h"

int
cmp(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	if (ret) return ret;

	cpu->regs.sr.n = (uint8_t)(cpu->regs.a - mem) >> 7;
	cpu->regs.sr.z = cpu->regs.a == mem;
	cpu->regs.sr.c = cpu->regs.a >= mem;

	return 0;
}

int
cpx(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	if (ret) return ret;

	cpu->regs.sr.n = (uint8_t)(cpu->regs.x - mem) >> 7;
	cpu->regs.sr.z = cpu->regs.x == mem;
	cpu->regs.sr.c = cpu->regs.x >= mem;

	return 0;
}

int
cpy(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	if (ret) return ret;

	cpu->regs.sr.n = (uint8_t)(cpu->regs.y - mem) >> 7;
	cpu->regs.sr.z = cpu->regs.y == mem;
	cpu->regs.sr.c = cpu->regs.y >= mem;

	return 0;
}

int
bit(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));
	if (ret) return ret;

	result = mem & cpu->regs.a;

	cpu->regs.sr.z = result == 0;
	cpu->regs.sr.n = (mem >> 7) & 0x1;
	cpu->regs.sr.v = (mem >> 6) & 0x1;

	return 0;
}

