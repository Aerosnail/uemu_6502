#pragma once

#include "architecture.h"
#include "memops.h"

int bra(struct cpu *cpu);
int bcc(struct cpu *cpu);
int bcs(struct cpu *cpu);
int bne(struct cpu *cpu);
int beq(struct cpu *cpu);
int bpl(struct cpu *cpu);
int bmi(struct cpu *cpu);
int bvc(struct cpu *cpu);
int bvs(struct cpu *cpu);

int bbr(struct cpu *cpu, int bit);
int bbs(struct cpu *cpu, int bit);
