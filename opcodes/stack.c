#include "stack.h"

int
pha(struct cpu *cpu)
{
	const uint16_t addr = 0x100 | cpu->regs.sp;
	int ret;

	ret = mem_write(cpu->bus, addr, &cpu->regs.a, sizeof(cpu->regs.a));
	cpu->regs.sp--;

	return ret;
}

int
pla(struct cpu *cpu)
{
	uint16_t addr;
	int ret;

	cpu->regs.sp++;
	addr = 0x100 | cpu->regs.sp;
	ret = mem_read(cpu->bus, addr, &cpu->regs.a, sizeof(cpu->regs.a));

	cpu->regs.sr.n = cpu->regs.a >> 7;
	cpu->regs.sr.z = cpu->regs.a == 0;

	return ret;
}

int
phx(struct cpu *cpu)
{
	const uint16_t addr = 0x100 | cpu->regs.sp;
	int ret;

	ret = mem_write(cpu->bus, addr, &cpu->regs.x, sizeof(cpu->regs.x));
	cpu->regs.sp--;

	return ret;
}

int
plx(struct cpu *cpu)
{
	uint16_t addr;
	int ret;

	cpu->regs.sp++;
	addr = 0x100 | cpu->regs.sp;
	ret = mem_read(cpu->bus, addr, &cpu->regs.x, sizeof(cpu->regs.x));

	cpu->regs.sr.n = cpu->regs.x >> 7;
	cpu->regs.sr.z = cpu->regs.x == 0;

	return ret;
}

int
phy(struct cpu *cpu)
{
	const uint16_t addr = 0x100 | cpu->regs.sp;
	int ret;

	ret = mem_write(cpu->bus, addr, &cpu->regs.y, sizeof(cpu->regs.y));
	cpu->regs.sp--;

	return ret;
}

int
ply(struct cpu *cpu)
{
	uint16_t addr;
	int ret;

	cpu->regs.sp++;
	addr = 0x100 | cpu->regs.sp;
	ret = mem_read(cpu->bus, addr, &cpu->regs.y, sizeof(cpu->regs.y));

	cpu->regs.sr.n = cpu->regs.y >> 7;
	cpu->regs.sr.z = cpu->regs.y == 0;

	return ret;
}

int
php(struct cpu *cpu)
{
	const uint16_t addr = 0x100 | cpu->regs.sp;
	uint8_t status;
	int ret;

	status = !!cpu->regs.sr.n << 7
		   | !!cpu->regs.sr.v << 6
		   | (1 << 5)
		   | (1 << 4)
		   | !!cpu->regs.sr.d << 3
		   | !!cpu->regs.sr.i << 2
		   | !!cpu->regs.sr.z << 1
		   | !!cpu->regs.sr.c;

	ret = mem_write(cpu->bus, addr, &status, sizeof(status));
	cpu->regs.sp--;

	return ret;
}

int
plp(struct cpu *cpu)
{
	uint16_t addr;
	uint8_t status;
	int ret;

	cpu->regs.sp++;
	addr = 0x100 | cpu->regs.sp;
	ret = mem_read(cpu->bus, addr, &status, sizeof(status));

	cpu->regs.sr.n = (status >> 7) & 0x1;
	cpu->regs.sr.v = (status >> 6) & 0x1;
	cpu->regs.sr.d = (status >> 3) & 0x1;
	cpu->regs.sr.i = (status >> 2) & 0x1;
	cpu->regs.sr.z = (status >> 1) & 0x1;
	cpu->regs.sr.c = status & 0x1;

	return ret;
}

