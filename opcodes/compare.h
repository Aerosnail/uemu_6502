#pragma once

#include "architecture.h"
#include "memops.h"

int cmp(struct cpu *cpu, enum addr_mode addr_mode);
int cpx(struct cpu *cpu, enum addr_mode addr_mode);
int cpy(struct cpu *cpu, enum addr_mode addr_mode);
int bit(struct cpu *cpu, enum addr_mode addr_mode);
