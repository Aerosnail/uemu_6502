#include "arith.h"

int
adc(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));

	if (cpu->regs.sr.d) {
		/* Convert operands from BCD to binary */
		result = (cpu->regs.a >> 4) * 10 + (cpu->regs.a & 0xF)
			   + (mem >> 4) * 10 + (mem & 0xF)
			   + cpu->regs.sr.c;

		/* Convert back to BCD */
		result = (result / 10 % 10) << 4 | (result % 10);
	} else {
		result = cpu->regs.a + mem + cpu->regs.sr.c;
	}

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.sr.c = result < cpu->regs.a || (cpu->regs.sr.c && result == cpu->regs.a);
	cpu->regs.sr.v = !((mem ^ cpu->regs.a) >> 7) && ((result ^ cpu->regs.a) >> 7);
	cpu->regs.a = result;

	return ret;
}

int
sbc(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t mem;
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &mem, sizeof(mem));

	if (cpu->regs.sr.d) {
		/* Convert operands from BCD to binary */
		result = (cpu->regs.a >> 4) * 10 + (cpu->regs.a & 0xF)
			   - (mem >> 4) * 10 + (mem & 0xF)
			   - !cpu->regs.sr.c;

		/* Convert back to BCD */
		result = (result / 10 % 10) << 4 | (result % 10);
	} else {
		result = cpu->regs.a - mem - !cpu->regs.sr.c;
	}

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.sr.c = result < cpu->regs.a || (cpu->regs.sr.c && result == cpu->regs.a);
	cpu->regs.sr.v = ((mem ^ cpu->regs.a) >> 7) && ((result ^ cpu->regs.a) >> 7);
	cpu->regs.a = result;

	return ret;
}

int
inc(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &result, sizeof(result));
	result = result + 1;
	ret |= mem_write(cpu->bus, addr, &result, sizeof(result));

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;

	return ret;
}

int
inca(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.a + 1;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return 0;
}

int
inx(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.x + 1;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.x = result;

	return 0;
}

int
iny(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.y + 1;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.y = result;

	return 0;
}

int
dec(struct cpu *cpu, enum addr_mode addr_mode)
{
	const uint16_t addr = mem_resolve(cpu, addr_mode);
	uint8_t result;
	int ret;

	ret = mem_read(cpu->bus, addr, &result, sizeof(result));
	result = result - 1;
	ret |= mem_write(cpu->bus, addr, &result, sizeof(result));

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;

	return ret;
}

int
deca(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.a - 1;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.a = result;

	return 0;
}

int
dex(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.x - 1;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.x = result;

	return 0;
}

int
dey(struct cpu *cpu)
{
	uint8_t result;

	result = cpu->regs.y - 1;

	cpu->regs.sr.n = result >> 7;
	cpu->regs.sr.z = result == 0;
	cpu->regs.y = result;

	return 0;
}
