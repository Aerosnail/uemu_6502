#pragma once

#include "architecture.h"

int pha(struct cpu *cpu);
int pla(struct cpu *cpu);
int phx(struct cpu *cpu);
int plx(struct cpu *cpu);
int phy(struct cpu *cpu);
int ply(struct cpu *cpu);
int php(struct cpu *cpu);
int plp(struct cpu *cpu);
