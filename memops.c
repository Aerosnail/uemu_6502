#include <assert.h>
#include "memops.h"

static int fetch_next(struct cpu *cpu, void *dst, size_t count);
static inline void static_memcpy(void *restrict dst, const void *restrict src, size_t len);

uint16_t
mem_resolve_cond(struct cpu *cpu, enum addr_mode addr_mode, int cond)
{
	if (cond) return mem_resolve(cpu, addr_mode);
	switch (addr_mode) {
	case ADDR_INVALID:
	case ADDR_ACC:
		break;
	case ADDR_IMM:
	case ADDR_REL:
	case ADDR_ZP:
	case ADDR_ZP_X:
	case ADDR_ZP_Y:
	case ADDR_IND_ZP:
	case ADDR_IND_ZP_X:
	case ADDR_IND_ZP_Y:
		cpu->instr_size += 1;
		break;
	case ADDR_ABS:
	case ADDR_ABS_X:
	case ADDR_ABS_Y:
	case ADDR_IND:
		cpu->instr_size += 2;
		break;
	}
	return 0;
}

uint16_t
mem_resolve(struct cpu *cpu, enum addr_mode addr_mode)
{
	uint16_t addr = 0;
	uint16_t ind;
	int8_t offset;
	const struct register_file *regs = &cpu->regs;

	switch (addr_mode) {
	case ADDR_ACC:
	case ADDR_INVALID:
		assert(0);
		break;

	case ADDR_IMM:      /* immediate */
		cpu->instr_size++;
		addr = regs->pc;
		break;

	case ADDR_REL:      /* Relative to current PC */
		fetch_next(cpu, &offset, 1);
		addr = regs->pc + offset;
		break;

	case ADDR_ZP:       /* zero page */
		fetch_next(cpu, &addr, 1);
		break;

	case ADDR_ZP_X:     /* zero page + X */
		fetch_next(cpu, &addr, 1);
		addr = (addr + regs->x) & 0xFF;
		break;

	case ADDR_ZP_Y:     /* zero page + Y */
		fetch_next(cpu, &addr, 1);
		addr = (addr + regs->y) & 0xFF;
		break;

	case ADDR_ABS_X:    /* absolute + X */
		fetch_next(cpu, &addr, 2);
		addr += regs->x;
		break;

	case ADDR_ABS_Y:    /* absolute + Y */
		fetch_next(cpu, &addr, 2);
		addr += regs->y;
		break;

	case ADDR_IND_ZP:
		fetch_next(cpu, &addr, 1);
		mem_read(cpu->bus, addr, &addr, 2);
		break;

	case ADDR_IND_ZP_X: /* (zero page + X) */
		fetch_next(cpu, &addr, 1);
		if (addr + regs->x == 0xFF) {
			/* Prevent zero-page boundary crossing */
			mem_read(cpu->bus, 0xFF, &addr, 1);
			mem_read(cpu->bus, 0x00, (uint8_t*)(&addr) + 1, 1);
		} else {
			mem_read(cpu->bus, (addr + regs->x) & 0xFF, &addr, 2);
		}
		break;

	case ADDR_IND_ZP_Y: /* (zero page) + Y */
		fetch_next(cpu, &addr, 1);
		if (addr == 0xFF) {
			/* Prevent zero-page boundary crossing */
			mem_read(cpu->bus, 0xFF, &addr, 1);
			mem_read(cpu->bus, 0x00, (uint8_t*)(&addr) + 1, 1);
		} else {
			mem_read(cpu->bus, addr, &addr, 2);
		}
		addr += regs->y;
		break;

	case ADDR_ABS:      /* absolute */
		fetch_next(cpu, &addr, 2);
		break;

	case ADDR_IND:      /* indirect */
		fetch_next(cpu, &ind, 2);
		if ((ind & 0xFF) == 0xFF) {
			/* Prevent page boundary crossing */
			mem_read(cpu->bus, ind, &addr, 1);
			mem_read(cpu->bus, ind & ~0xFF, (uint8_t*)(&addr) + 1, 1);
		} else {
			mem_read(cpu->bus, ind, &addr, 2);
		}
		break;
	}

	return addr;
}

static int
fetch_next(struct cpu *cpu, void *dst, size_t count)
{
	const uint16_t addr = cpu->regs.pc + cpu->instr_size;
	int ret = 0;

	assert(cpu->instr_size + count < MAX_INSTR_SIZE);

	static_memcpy(dst, cpu->exec_cache + cpu->instr_size + 1, count);
	cpu->instr_size += count;

	return ret;
}

inline static void
static_memcpy(void *restrict dst, const void *restrict src, size_t len)
{
	size_t i;

	for (i = 0; i < len; i++) {
		((uint8_t *restrict)dst)[i] = ((uint8_t *restrict)src)[i];
		__asm(""); /* Prevents this from turning into a memcpy */
	}
}
