#pragma once

#include "architecture.h"

int gdb_start_stub(struct cpu *cpu, uint16_t port);
int gdb_stop_stub(struct cpu *cpu);

int gdb_notify_stop_reason(struct cpu *cpu, int signal);
