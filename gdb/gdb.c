#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#include <utils.h>

#include "gdb.h"
#include "cpu.h"

struct gdb_command_t {
	const char *cmd;
	int (*fcn)(void*, const char*, size_t, char*, size_t);
};


static int gdb_get_stop_reason(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_step(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_read_reg(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_write_reg(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_read_regs(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_write_regs(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_continue(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_break(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_read_mem(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_write_mem(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_add_breakpoint(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_del_breakpoint(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_qxfer_features_read(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_qsupported(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_get_reg_info(void *ctx, const char *args, size_t len, char *resp, size_t resplen);
static int gdb_get_host_info(void *ctx, const char *args, size_t len, char *resp, size_t resplen);

extern const char _binary_target_xml[];
extern unsigned long _binary_target_xml_size;

static const struct gdb_command_t cmds[] = {
	{ .cmd = "?", .fcn = gdb_get_stop_reason },
	{ .cmd = "p", .fcn = gdb_read_reg },
	{ .cmd = "P", .fcn = gdb_write_reg },
	{ .cmd = "g", .fcn = gdb_read_regs },
	{ .cmd = "G", .fcn = gdb_write_regs },
	{ .cmd = "m", .fcn = gdb_read_mem },
	{ .cmd = "M", .fcn = gdb_write_mem },
	{ .cmd = "z", .fcn = gdb_del_breakpoint },
	{ .cmd = "Z", .fcn = gdb_add_breakpoint },
	{ .cmd = "s", .fcn = gdb_step },
	{ .cmd = "c", .fcn = gdb_continue },
	{ .cmd = "BREAK", .fcn = gdb_break },
	{ .cmd = "qSupported:", .fcn = gdb_qsupported },
	{ .cmd = "qHostInfo", .fcn = gdb_get_host_info },
	{ .cmd = "qRegisterInfo", .fcn = gdb_get_reg_info },
	{ .cmd = "qXfer:features:read", .fcn = gdb_qxfer_features_read },
};

int
gdb_start_stub(struct cpu *cpu, uint16_t port)
{
	int ret;
	size_t i;

	/* Start stub */
	ret = gdbstub_init(&cpu->gdb, port);
	if (ret) return ret;

	/* Register commands */
	for (i = 0; i < LEN(cmds); i++) {
		gdbstub_register_cmd(cpu->gdb, cmds[i].cmd, cmds[i].fcn, cpu);
	}

	return ret;
}

int
gdb_stop_stub(struct cpu *cpu)
{
	size_t i;
	int ret;

	if (!cpu->gdb) return 0;

	/* Unregister commands */
	for (i = 0; i < LEN(cmds); i++) {
		gdbstub_unregister_cmd(cpu->gdb, cmds[i].cmd);
	}

	/* Stop stub */
	ret = gdbstub_deinit(&cpu->gdb);

	return ret;

}

int
gdb_notify_stop_reason(struct cpu *cpu, int signal)
{
	char resp[64];
	size_t offset;
	uint8_t status;

	if (!cpu->gdb) return 0;
	status = !!cpu->regs.sr.n << 7
		   | !!cpu->regs.sr.v << 6
		   | !!cpu->regs.sr.d << 3
		   | !!cpu->regs.sr.i << 2
		   | !!cpu->regs.sr.z << 1
		   | !!cpu->regs.sr.c;


	offset = 0;
	offset += snprintf(resp, sizeof(resp), "T%02x", signal);
	offset += snprintf(resp + offset, sizeof(resp) - offset, "0:%02x;", cpu->regs.a);
	offset += snprintf(resp + offset, sizeof(resp) - offset, "1:%02x;", cpu->regs.x);
	offset += snprintf(resp + offset, sizeof(resp) - offset, "2:%02x;", cpu->regs.y);
	offset += snprintf(resp + offset, sizeof(resp) - offset, "3:%02x;", cpu->regs.sp);
	offset += snprintf(resp + offset, sizeof(resp) - offset, "4:%02x;", status);
	offset += snprintf(resp + offset, sizeof(resp) - offset, "5:%02x%02x", cpu->regs.pc & 0xFF, cpu->regs.pc >> 8);

	gdbstub_notify(cpu->gdb, resp, strlen(resp));

	return 0;
}

/* Static functions {{{ */
static int
gdb_get_stop_reason(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	size_t offset;
	struct cpu *cpu = (struct cpu*)ctx;
	if (!resp || !resplen) return EINVAL;
	uint8_t status;

	(void)args;
	(void)len;

	assert(cpu);
	if (cpu->tid) {
		cpu->thread_running = false;
		pthread_join(cpu->tid, NULL);
		cpu->tid = 0;
	}

	status = !!cpu->regs.sr.n << 7
		   | !!cpu->regs.sr.v << 6
		   | !!cpu->regs.sr.d << 3
		   | !!cpu->regs.sr.i << 2
		   | !!cpu->regs.sr.z << 1
		   | !!cpu->regs.sr.c;


	offset = 0;
	offset += snprintf(resp, resplen, "T%02x", SIGTRAP);
	offset += snprintf(resp + offset, resplen - offset, "0:%02x;", cpu->regs.a);
	offset += snprintf(resp + offset, resplen - offset, "1:%02x;", cpu->regs.x);
	offset += snprintf(resp + offset, resplen - offset, "2:%02x;", cpu->regs.y);
	offset += snprintf(resp + offset, resplen - offset, "3:%02x;", cpu->regs.sp);
	offset += snprintf(resp + offset, resplen - offset, "4:%02x;", status);
	offset += snprintf(resp + offset, resplen - offset, "5:%02x%02x", cpu->regs.pc & 0xFF, cpu->regs.pc >> 8);

	return 0;
}

static int
gdb_step(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	uint16_t addr;
	char *addr_end;

	(void)args;
	(void)len;

	assert(ctx);
	assert(resp);

	addr = strtol(args, &addr_end, 16);
	if (addr_end != args) {
		cpu->regs.pc = addr;
	}

	cpu_step(cpu);

	return gdb_get_stop_reason(ctx, args, len, resp, resplen);
}

static int
gdb_read_reg(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	unsigned int reg_idx = 0;
	uint8_t status;

	assert(ctx);
	if (!args || !len) return EINVAL;

	sscanf(args, "%x", &reg_idx);

	switch (reg_idx) {
	case 0x00:
		snprintf(resp, resplen, "%02x", cpu->regs.a);
		break;
	case 0x01:
		snprintf(resp, resplen, "%02x", cpu->regs.x);
		break;
	case 0x02:
		snprintf(resp, resplen, "%02x", cpu->regs.y);
		break;
	case 0x03:
		snprintf(resp, resplen, "%02x", cpu->regs.sp);
		break;
	case 0x04:
		status = !!cpu->regs.sr.n << 7
			   | !!cpu->regs.sr.v << 6
			   | !!cpu->regs.sr.d << 3
			   | !!cpu->regs.sr.i << 2
			   | !!cpu->regs.sr.z << 1
			   | !!cpu->regs.sr.c;
		snprintf(resp, resplen, "%02x", status);
		break;
	case 0x05:
		snprintf(resp, resplen, "%02x%02x", cpu->regs.pc & 0xFF, cpu->regs.pc >> 8);
		break;
	default:
		return EINVAL;
	}

	return 0;
}

static int
gdb_write_reg(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	unsigned int reg_idx = 0;
	unsigned int reg_val;

	assert(ctx);
	if (!args || !len) return EINVAL;
	if (cpu->tid) cpu_stop(cpu);
	if (cpu->tid) return EBUSY;

	sscanf(args, "%x=%x", &reg_idx, &reg_val);


	switch (reg_idx) {
	case 0x00:
		cpu->regs.a = reg_val;
		break;
	case 0x01:
		cpu->regs.x = reg_val;
		break;
	case 0x02:
		cpu->regs.y = reg_val;
		break;
	case 0x03:
		cpu->regs.sp = reg_val;
		break;
	case 0x04:
		cpu->regs.sr.n = (reg_val >> 7) & 0x1;
		cpu->regs.sr.v = (reg_val >> 6) & 0x1;
		cpu->regs.sr.d = (reg_val >> 3) & 0x1;
		cpu->regs.sr.i = (reg_val >> 2) & 0x1;
		cpu->regs.sr.z = (reg_val >> 1) & 0x1;
		cpu->regs.sr.c = reg_val & 0x1;
		break;
	case 0x05:
		cpu->regs.pc = (reg_val & 0xFF) << 8
		             | (reg_val & 0xFF00) >> 8;
		break;
	default:
		return EINVAL;
	}

	snprintf(resp, resplen, "OK");

	return 0;
}

static int
gdb_read_regs(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	uint8_t status;
	size_t offset;

	(void)args;
	(void)len;

	assert(ctx);
	if (!resp || !resplen) return EINVAL;

	status = !!cpu->regs.sr.n << 7
		   | !!cpu->regs.sr.v << 6
		   | !!cpu->regs.sr.d << 3
		   | !!cpu->regs.sr.i << 2
		   | !!cpu->regs.sr.z << 1
		   | !!cpu->regs.sr.c;

	offset = 0;
	offset += snprintf(resp + offset, resplen - offset, "%02x", cpu->regs.a);
	offset += snprintf(resp + offset, resplen - offset, "%02x", cpu->regs.x);
	offset += snprintf(resp + offset, resplen - offset, "%02x", cpu->regs.y);
	offset += snprintf(resp + offset, resplen - offset, "%02x", cpu->regs.sp);
	offset += snprintf(resp + offset, resplen - offset, "%02x", status);
	offset += snprintf(resp + offset, resplen - offset, "%02x%02x", cpu->regs.pc & 0xFF, cpu->regs.pc >> 8);

	return 0;
}

static int
gdb_write_regs(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	size_t offset;
	unsigned int tmp, tmp2;

	assert(ctx);
	if (!args) return EINVAL;
	if (cpu->tid) cpu_stop(cpu);
	if (cpu->tid) return EBUSY;
	offset = 0;

	/* Write A reg if available */
	if (offset >= len) return EINVAL;
	if (args[offset] != 'x') {
		if (sscanf(args + offset, "%02x", &tmp)) {
			cpu->regs.a = tmp;
		}
	}
	offset += 2;

	/* Write X reg if available */
	if (offset >= len) return EINVAL;
	if (args[offset] != 'x') {
		if (sscanf(args + offset, "%02x", &tmp)) {
			cpu->regs.x = tmp;
		}
	}
	offset += 2;

	/* Write Y reg if available */
	if (offset >= len) return EINVAL;
	if (args[offset] != 'x') {
		if (sscanf(args + offset, "%02x", &tmp)) {
			cpu->regs.y = tmp;
		}
	}
	offset += 2;

	/* Write status reg if available */
	if (offset >= len) return EINVAL;
	if (args[offset] != 'x') {
		if (sscanf(args + offset, "%02x", &tmp)) {
			cpu->regs.sr.n = (tmp >> 7) & 0x1;
			cpu->regs.sr.v = (tmp >> 6) & 0x1;
			cpu->regs.sr.d = (tmp >> 3) & 0x1;
			cpu->regs.sr.i = (tmp >> 2) & 0x1;
			cpu->regs.sr.z = (tmp >> 1) & 0x1;
			cpu->regs.sr.c = tmp & 0x1;
		}
	}
	offset += 2;

	/* Write SP reg if available */
	if (offset >= len) return EINVAL;
	if (args[offset] != 'x') {
		if (sscanf(args + offset, "%02x", &tmp)) {
			cpu->regs.sp = tmp;
		}
	}
	offset += 2;

	/* Write PC reg if available */
	if (offset >= len) return EINVAL;
	if (args[offset] != 'x') {
		if (sscanf(args + offset, "%02x%02x", &tmp, &tmp2)) {
			cpu->regs.pc = tmp | tmp2 << 8;
		}
	}
	offset += 4;

	if (resplen) resp[0] = 0;

	return 0;
}

static int
gdb_continue(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	assert(ctx);
	int ret;

	(void)args;
	(void)len;
	(void)resp;
	(void)resplen;

	ret = cpu_run(cpu);

	if (ret) return ret;
	return ENOMSG;
}

static int
gdb_break(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	assert(ctx);

	cpu_stop(cpu);

	return gdb_get_stop_reason(ctx, args, len, resp, resplen);
}

static int
gdb_read_mem(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	unsigned int mem_addr, mem_len;
	int chunksize;
	int argcount;
	uint8_t tmp;
	int ret;

	assert(ctx);
	if (!args || !len || !resp || !resplen) return EINVAL;

	argcount = sscanf(args, "%x,%x", &mem_addr, &mem_len);
	if (argcount != 2) return EINVAL;
	if (resplen < 2 * mem_len) return ENOMEM;

	for(; mem_len > 0; mem_len--, mem_addr++) {
		ret = mem_read(cpu->bus, mem_addr, &tmp, 1);
		if (ret) return ret;

		chunksize = snprintf(resp, resplen, "%02x", tmp);
		resp += chunksize;
		resplen -= chunksize;
	}

	return 0;
}

static int
gdb_write_mem(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	unsigned int mem_addr, mem_len;
	unsigned int tmp;
	int argcount;
	int ret;

	assert(ctx);
	if (!args || !len) return EINVAL;

	argcount = sscanf(args, "%x,%x", &mem_addr, &mem_len);
	if (argcount != 2) return EINVAL;

	while (*args++ != ',')
		;

	for(; mem_len > 0; mem_len--, mem_addr++) {
		/* Parse another byte */
		ret = sscanf(args, "%02x", &tmp);
		if (!ret) return EINVAL;
		args += 2;

		ret = mem_write(cpu->bus, mem_addr, &tmp, 1);
		if (ret) return ret;
	}

	if (resplen) resp[0] = 0;
	return 0;
}

static int
gdb_add_breakpoint(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	uint32_t type, addr, kind;
	int argcount;
	int ret = 0;

	assert(ctx);
	if (!args || !len) return EINVAL;

	argcount = sscanf(args, "%x,%x,%x", &type, &addr, &kind);
	if (argcount != 3) return EINVAL;

	switch (type) {
	case GDBSTUB_BKPT_SW:
	case GDBSTUB_BKPT_HW:
		ret = cpu_add_breakpoint(cpu, addr);
		if (!ret) snprintf(resp, resplen, "OK");
		break;

	default:
		return ENOSYS;
	}

	return ret;
}

static int
gdb_del_breakpoint(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	struct cpu *cpu = (struct cpu*)ctx;
	uint32_t type, addr, kind;
	int argcount;
	int ret = 0;

	assert(ctx);
	if (!args || !len) return EINVAL;

	argcount = sscanf(args, "%x,%x,%x", &type, &addr, &kind);
	if (argcount != 3) return EINVAL;

	switch (type) {
	case GDBSTUB_BKPT_SW:
	case GDBSTUB_BKPT_HW:
		ret = cpu_del_breakpoint(cpu, addr);
		if (!ret) snprintf(resp, resplen, "OK");
		break;

	default:
		return ENOSYS;
	}

	return ret;
}

static int
gdb_qxfer_features_read(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	unsigned int annex_offset, annex_len;
	int argcount;

	assert(ctx);
	if (!args || !len) return EINVAL;

	argcount = sscanf(args, ":%*[^:]:%x,%x", &annex_offset, &annex_len);
	if (!argcount) return EINVAL;
	/* Limit the chunksize to the size of the output buffer */
	if (resplen < annex_len) {
		annex_len = resplen;
	}

	if (strbegins(args, ":target.xml:")) {
		if (annex_offset >= _binary_target_xml_size) {
			snprintf(resp, resplen, "l");
		} else {
			snprintf(resp, resplen, "m");
			annex_len = MIN(_binary_target_xml_size - annex_offset, annex_len);

			/* Zero-terminate */
			resp[strlen(resp) + annex_len] = 0;

			memcpy(resp + strlen(resp), _binary_target_xml + annex_offset, annex_len);

		}
		return 0;
	}

	if (resplen) resp[0] = 0;

	return ENOSYS;
}

static int
gdb_qsupported(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	if (!resp || !resplen) return EINVAL;

	(void)ctx;
	(void)args;
	(void)len;

	resp += snprintf(resp, resplen, "PacketSize=8192;qXfer:features:read+;QStartNoAckMode");

	return 0;
}

static int
gdb_get_reg_info(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	unsigned int reg_idx = 0;

	assert(ctx);
	if (!args || !len) return EINVAL;

	sscanf(args, "%x", &reg_idx);

	switch (reg_idx) {
	case 0x00:
		snprintf(resp, resplen, "name:a;bitsize:8;offset:0;encoding:uint;format:hex;set:General Purpose Registers;");
		break;
	case 0x01:
		snprintf(resp, resplen, "name:x;bitsize:8;offset:1;encoding:uint;format:hex;set:General Purpose Registers;");
		break;
	case 0x02:
		snprintf(resp, resplen, "name:y;bitsize:8;offset:2;encoding:uint;format:hex;set:General Purpose Registers;");
		break;
	case 0x03:
		snprintf(resp, resplen, "name:sp;bitsize:8;offset:3;encoding:uint;format:hex;set:General Purpose Registers;generic:sp;");
		break;
	case 0x04:
		snprintf(resp, resplen, "name:f;bitsize:8;offset:4;encoding:uint;format:binary;set:General Purpose Registers;generic:flags;");
		break;
	case 0x05:
		snprintf(resp, resplen, "name:pc;bitsize:16;offset:5;encoding:uint;format:hex;set:General Purpose Registers;generic:pc;");
		break;
	default:
		return EINVAL;
	}

	return 0;
}

static int
gdb_get_host_info(void *ctx, const char *args, size_t len, char *resp, size_t resplen)
{
	if (!resp || !resplen) return EINVAL;

	(void)ctx;
	(void)args;
	(void)len;

	/* triple is i386-unknown-none. This is because llvm mainline does not
	 * support the 6502 architecture, and while you can read registers with lldb
	 * regardless of the triple, you cannot write to them unless that triple is
	 * known by lldb. i386 is good because it is little endian and it has a min
	 * instruction length of 1, so breakpoints do not have to be aligned to
	 * 16/32 bits */
	resp += snprintf(resp, resplen, "triple:693338362d756e6b6e6f776e2d6e6f6e65;endian:little;ptrsize:4;");

	return 0;
}
/* }}} */
