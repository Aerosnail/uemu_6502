#pragma once

#include "architecture.h"

int execute(struct cpu *cpu, uint8_t instr);
