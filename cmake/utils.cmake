function(embed_blobs out_var)
	set(result)
	foreach(IN_FILE ${ARGN})
		get_filename_component(SRC_FILE ${IN_FILE} NAME)
		set(OUT_FILE "${CMAKE_CURRENT_BINARY_DIR}/${SRC_FILE}.c")

		add_custom_command(
			OUTPUT ${OUT_FILE}
			COMMAND ${CMAKE_COMMAND} -DIN_FILE=${IN_FILE} -DOUT_FILE=${OUT_FILE} -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/EmbedBlob.cmake
			DEPENDS ${IN_FILE}
			WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${SRC_DIR}"
		)

		list(APPEND result ${OUT_FILE})
	endforeach()
	set(${out_var} "${result}" PARENT_SCOPE)
endfunction()
