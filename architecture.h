#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>

#include <clock.h>
#include <interrupts.h>
#include <gpio.h>
#include <module.h>
#include <gdb/gdbstub.h>

#define RST_VECTOR_ADDR 0xFFFC
#define NMI_VECTOR_ADDR 0xFFFA
#define IRQ_VECTOR_ADDR 0xFFFE
#define BRK_VECTOR_ADDR IRQ_VECTOR_ADDR

#define MAX_INSTR_SIZE 3

typedef uint32_t opcode_t;

enum interrupt {
	INT_RST = 0,
	INT_NMI = 1,
	INT_IRQ = 2
};

struct flags {
	uint_fast8_t n;  /* negative */
	uint_fast8_t v;  /* overflow */
	uint_fast8_t b;  /* break */
	uint_fast8_t d;  /* decimal */
	uint_fast8_t i;  /* interrupt disable */
	uint_fast8_t z;  /* zero */
	uint_fast8_t c;  /* carry */
};

struct register_file {
	uint8_t a, x, y;
	uint8_t sp;
	uint16_t pc;
	struct flags sr;
};

struct cpu {
	char *name;
	MemBus *bus;
	InterruptManager *int_mgr;
	ClockPeriph *clk;

	struct register_file regs;
	int instr_size;
	bool stp, wai;
	int breakpoint;
	uint8_t exec_cache[MAX_INSTR_SIZE];

	bool thread_running;
	pthread_t tid;

	struct gdbstub *gdb;
};
