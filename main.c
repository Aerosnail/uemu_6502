#include <string.h>
#include <stdlib.h>

#include <module.h>
#include <gdb/gdbstub.h>

#include "architecture.h"
#include "cpu.h"
#include "commands.h"
#include "devicetree.h"

static const char *_compatible[] = {
	"mos,6502",
	"mos,65c02",
	NULL
};

__global
ModuleInfo __info = {
	.name = "6502",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible
};

__global int
__init(void)
{
	return 0;
}

__global int
__parse_dtb_prop(void **dst, const struct dtb_property *prop, find_phandle_t find_phandle, void *find_phandle_ctx)
{
	struct cpu_specs *specs;

	if (!*dst) *dst = calloc(1, sizeof(*specs));
	specs = *dst;

	return parse_dtb_prop(specs, prop, find_phandle, find_phandle_ctx);
}

__global void*
__create_instance(ModuleSpecs *specs)
{
	struct cpu_specs *custom_specs = specs->ctx;
	struct cpu *self;
	Clock *clock;

	if (!custom_specs) return NULL;

	clock = clock_get(custom_specs->clock);
	if (!clock) return NULL;

	self = cpu_init(specs->bus, clock, specs->name);
	if (!self) return NULL;

	self->name = strdup(specs->name);
	register_commands(specs->name, self);

	return self;
}

__global int
__delete_instance(void *ctx)
{
	struct cpu *self = (struct cpu*)ctx;
	int ret = 0;

	if (self->gdb) ret |= gdbstub_deinit(&self->gdb);
	ret |= unregister_commands(self->name);
	free(self->name);
	ret |= cpu_deinit(self);

	return ret;
}

__global int
__deinit(void)
{
	return 0;
}
