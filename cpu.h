#pragma once

#include "module.h"

struct cpu *cpu_init(MemBus *bus, Clock *clk, const char *name);
int cpu_deinit(struct cpu *cpu);
int cpu_reset(struct cpu *cpu);
int cpu_step(struct cpu *cpu);
int cpu_run(struct cpu *cpu);
int cpu_stop(struct cpu *cpu);

int cpu_add_breakpoint(struct cpu *cpu, uint16_t addr);
int cpu_del_breakpoint(struct cpu *cpu, uint16_t addr);
