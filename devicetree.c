#include <assert.h>
#include <errno.h>
#include <string.h>

#include <dtb/node.h>
#include <utils.h>

#include "devicetree.h"

enum props {
	PROP_CLOCK,
	PROP_COUNT
};

static const char *_prop_names[] = {
	[PROP_CLOCK] = "clocks"
};

int
parse_dtb_prop(struct cpu_specs *dst, const struct dtb_property *prop,
                   find_phandle_t find_phandle, void *find_phandle_ctx)
{
	const struct dtb_node *phandle_node;
	const struct dtb_property *clock_cells_prop;
	uint32_t phandle, clock_cells;
	int i;

	(void)find_phandle;
	(void)find_phandle_ctx;

	for (i = 0; i < PROP_COUNT; i++) {
		if (!strcmp(prop->name, _prop_names[i])) {
			break;
		}
	}

	switch (i) {
	case PROP_CLOCK:
		/* Get phandle */
		dtb_property_get_u32(&phandle, prop, 0, 1);

		/* Get #clock-cells value for module with the given phandle */
		phandle_node = find_phandle(find_phandle_ctx, phandle);
		clock_cells_prop = dtb_node_find_property(phandle_node, "#clock-cells");
		if (!clock_cells_prop) return ENOKEY;

		dtb_property_get_u32(&clock_cells, clock_cells_prop, 0, 1);
		assert(!clock_cells);

		dst->clock = phandle_node->name;
		break;

	default:
		return ENOKEY;
	}

	return 0;
}

