#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <log/log.h>

#include "architecture.h"
#include "cpu.h"
#include "decoder.h"
#include "gdb/gdb.h"

static int cpu_int_enter(struct cpu *self, enum interrupt irq);
static void *cpu_thread(void *args);

struct cpu*
cpu_init(MemBus *bus, Clock *clk, const char *name)
{
	struct cpu *cpu = calloc(1, sizeof(*cpu));
	if (!cpu) return NULL;

	cpu->bus = bus;
	cpu->int_mgr = int_mgr_get(name);
	if (!cpu->int_mgr) return NULL;
	cpu->clk = clock_add_peripheral(clk);
	if (!cpu->clk) return NULL;

	int_set_priority(cpu->int_mgr, INT_RST, 0);
	int_set_priority(cpu->int_mgr, INT_NMI, 1);
	int_set_priority(cpu->int_mgr, INT_IRQ, 2);

	cpu->breakpoint = -1;
	cpu_reset(cpu);

	return cpu;
}

int
cpu_deinit(struct cpu *cpu)
{
	if (cpu->thread_running) {
		cpu->thread_running = false;
		pthread_join(cpu->tid, NULL);
	}

	clock_del_peripheral(cpu->clk);
	free(cpu);
	return 0;
}

int
cpu_reset(struct cpu *cpu)
{
	memset(&cpu->regs, 0, sizeof(cpu->regs));

	cpu->regs.sp = 0xFD;

	mem_read(cpu->bus, RST_VECTOR_ADDR, &cpu->regs.pc, sizeof(cpu->regs.pc));

	cpu->stp = false;
	cpu->wai = false;

	return 0;
}

int
cpu_step(struct cpu *cpu)
{
	uint8_t opcode;
	int ret;
	int irq;

	/* Handle pending IRQ */
	irq = int_get_next_pending(cpu->int_mgr);
	if (__builtin_expect(irq > 0, 0)) {
		cpu_int_enter(cpu, irq);
	}

	if (cpu->stp || cpu->wai) {
		/* Processor halted */
		return 0;
	}

	/* Fetch instruction */
	ret = mem_execute(cpu->bus, cpu->regs.pc, cpu->exec_cache, sizeof(cpu->exec_cache));
	opcode = cpu->exec_cache[0];
	if (ret) return ret;

	/* Increment PC */
	cpu->regs.pc++;
	cpu->instr_size = 0;

	/* Decode and execute */
	ret = execute(cpu, opcode);

	/* Adjust PC accounting for variable instruction size */
	cpu->regs.pc += cpu->instr_size;

	/* Wait for instruction to complete execution */
	clock_wait(cpu->clk, 1);

	return ret;
}

int
cpu_run(struct cpu *cpu)
{
	if (cpu->tid) return EBUSY;

	cpu->thread_running = true;
	pthread_create(&cpu->tid, NULL, cpu_thread, cpu);

	if (!cpu->tid) {
		cpu->thread_running = false;
		return ECHILD;
	}

	return 0;
}

int
cpu_stop(struct cpu *cpu)
{
	if (!cpu->tid) return ECHILD;

	cpu->thread_running = false;
	pthread_join(cpu->tid, NULL);

	cpu->tid = 0;

	return 0;
}

int
cpu_add_breakpoint(struct cpu *cpu, uint16_t addr)
{
	if (cpu->breakpoint >= 0) return ENOMEM;
	cpu->breakpoint = addr;
	return 0;
}

int
cpu_del_breakpoint(struct cpu *cpu, uint16_t addr)
{
	if (cpu->breakpoint != addr) return ENOENT;
	cpu->breakpoint = -1;
	return 0;
}

/* Static functions {{{ */
static int
cpu_int_enter(struct cpu *cpu, enum interrupt irq)
{
	uint16_t stack;
	uint8_t status;

	status = !!cpu->regs.sr.n << 7
	       | !!cpu->regs.sr.v << 6
	       | !!cpu->regs.sr.d << 3
	       | !!cpu->regs.sr.i << 2
	       | !!cpu->regs.sr.z << 1
	       | !!cpu->regs.sr.c;

	stack = 0x100 | (uint8_t)(cpu->regs.sp - 1);
	mem_write(cpu->bus, stack, &cpu->regs.pc, sizeof(cpu->regs.pc));
	cpu->regs.sp -= 2;

	stack = 0x100 | cpu->regs.sp;
	mem_write(cpu->bus, stack, &status, sizeof(status));
	cpu->regs.sp--;

	switch (irq) {
	case INT_RST:
		cpu_reset(cpu);
		return 0;
	case INT_NMI:
		cpu->wai = false;
		mem_read(cpu->bus, NMI_VECTOR_ADDR, &cpu->regs.pc, 2);
		break;
	case INT_IRQ:
		cpu->wai = false;
		mem_read(cpu->bus, IRQ_VECTOR_ADDR, &cpu->regs.pc, 2);
		break;
	default:
		break;
	}

	return 0;
}

static void
*cpu_thread(void *args)
{
	struct cpu *self = (struct cpu*)args;

	while (self->thread_running) {
		if (self->regs.pc == self->breakpoint) break;
		cpu_step(self);
	}

	gdb_notify_stop_reason(self, SIGTRAP);

	self->thread_running = 0;
	return NULL;
}
/* }}} */
