#include <errno.h>

#include "architecture.h"
#include "decoder.h"
#include "memops.h"
#include "opcodes/arith.h"
#include "opcodes/branch.h"
#include "opcodes/compare.h"
#include "opcodes/load_store.h"
#include "opcodes/logic.h"
#include "opcodes/misc.h"
#include "opcodes/set_clear.h"
#include "opcodes/shift.h"
#include "opcodes/stack.h"
#include "opcodes/subroutines.h"
#include "opcodes/transfer.h"

static const enum addr_mode group_one_modes[] = {
	[0x00] = ADDR_IND_ZP_X,
	[0x01] = ADDR_ZP,
	[0x02] = ADDR_IMM,
	[0x03] = ADDR_ABS,
	[0x04] = ADDR_IND_ZP_Y,
	[0x05] = ADDR_ZP_X,
	[0x06] = ADDR_ABS_Y,
	[0x07] = ADDR_ABS_X,

	[0x08] = ADDR_IND       /* 65C02 marker */
};

static const enum addr_mode group_two_modes[8] = {
	[0x00] = ADDR_IMM,
	[0x01] = ADDR_ZP,
	[0x02] = ADDR_ACC,
	[0x03] = ADDR_ABS,
	[0x04] = ADDR_ZP,
	[0x05] = ADDR_ZP_X,
	[0x06] = ADDR_INVALID,
	[0x07] = ADDR_ABS_X
};

static const enum addr_mode group_three_modes[8] = {
	[0x00] = ADDR_IMM,
	[0x01] = ADDR_ZP,
	[0x02] = ADDR_INVALID,
	[0x03] = ADDR_ABS,
	[0x04] = ADDR_INVALID,
	[0x05] = ADDR_ZP_X,
	[0x06] = ADDR_INVALID,
	[0x07] = ADDR_ABS_X
};

int
execute(struct cpu *cpu, uint8_t opcode)
{
	uint_fast8_t a, b, c;
	enum addr_mode addr_mode;

	/* Use LUT for single byte/irregular instructions */
	switch (opcode) {
	case 0x00: return brk(cpu);
	case 0x10: return bpl(cpu);
	case 0x20: return jsr(cpu, ADDR_ABS);
	case 0x30: return bmi(cpu);
	case 0x40: return rti(cpu);
	case 0x50: return bvc(cpu);
	case 0x60: return rts(cpu);
	case 0x70: return bvs(cpu);
	case 0x80: return bra(cpu);
	case 0x90: return bcc(cpu);
	case 0xb0: return bcs(cpu);
	case 0xd0: return bne(cpu);
	case 0xf0: return beq(cpu);

	case 0x08: return php(cpu);
	case 0x18: return clc(cpu);
	case 0x28: return plp(cpu);
	case 0x38: return sec(cpu);
	case 0x48: return pha(cpu);
	case 0x58: return cli(cpu);
	case 0x68: return pla(cpu);
	case 0x78: return sei(cpu);
	case 0x88: return dey(cpu);
	case 0x98: return tya(cpu);
	case 0xa8: return tay(cpu);
	case 0xb8: return clv(cpu);
	case 0xc8: return iny(cpu);
	case 0xd8: return cld(cpu);
	case 0xe8: return inx(cpu);
	case 0xf8: return sed(cpu);

	case 0x89: return bit(cpu, ADDR_IMM);

	case 0x1a: return inca(cpu);
	case 0x3a: return deca(cpu);
	case 0x5a: return phy(cpu);
	case 0x7a: return ply(cpu);
	case 0x8a: return txa(cpu);
	case 0x9a: return txs(cpu);
	case 0xaa: return tax(cpu);
	case 0xba: return tsx(cpu);
	case 0xca: return dex(cpu);
	case 0xda: return phx(cpu);
	case 0xea: return nop(cpu);
	case 0xfa: return plx(cpu);

	default:
		break;
	}

	/* Split opcode into function + addressing + family */
	a = (opcode >> 5) & 0x7;
	b = (opcode >> 2) & 0x7;
	c = opcode & 0x3;

	/* Adjust 65C02 zero-page indirect */
	if (b == 0x04 && c == 0x02) {
		c = 0x01;
		b = 0x08;
	}

	switch (c) {
	case 0x00:  /* Group Three */
		addr_mode = group_three_modes[b];
		switch (a) {
		case 0x00:
			/* TODO implement */
			//return tsb(cpu, addr_mode);
			break;
		case 0x01:
			return bit(cpu, addr_mode);
		case 0x02:
			return jmp(cpu, addr_mode);
		case 0x03:
			return jmp(cpu, ADDR_IND);
		case 0x04:
			return sty(cpu, addr_mode);
		case 0x05:
			return ldy(cpu, addr_mode);
		case 0x06:
			return cpy(cpu, addr_mode);
		case 0x07:
			return cpx(cpu, addr_mode);
		}
		break;

	case 0x01:  /* Group One */
		addr_mode = group_one_modes[b];
		switch (a) {
		case 0x00:
			return ora(cpu, addr_mode);
		case 0x01:
			return and(cpu, addr_mode);
		case 0x02:
			return eor(cpu, addr_mode);
		case 0x03:
			return adc(cpu, addr_mode);
		case 0x04:
			return sta(cpu, addr_mode);
		case 0x05:
			return lda(cpu, addr_mode);
		case 0x06:
			return cmp(cpu, addr_mode);
		case 0x07:
			return sbc(cpu, addr_mode);
		}
		break;

	case 0x02:  /* Group Two */
		addr_mode = group_two_modes[b];
		switch (a) {
		case 0x00:
			if (addr_mode == ADDR_ACC) return asl_acc(cpu);
			return asl(cpu, addr_mode);
		case 0x01:
			if (addr_mode == ADDR_ACC) return rol_acc(cpu);
			return rol(cpu, addr_mode);
		case 0x02:
			if (addr_mode == ADDR_ACC) return lsr_acc(cpu);
			return lsr(cpu, addr_mode);
		case 0x03:
			if (addr_mode == ADDR_ACC) return ror_acc(cpu);
			return ror(cpu, addr_mode);
		case 0x04:
			if (addr_mode == ADDR_ZP_X) addr_mode = ADDR_ZP_Y;
			return stx(cpu, addr_mode);
		case 0x05:
			if (addr_mode == ADDR_ZP_X) addr_mode = ADDR_ZP_Y;
			else if (addr_mode == ADDR_ABS_X) addr_mode = ADDR_ABS_Y;
			return ldx(cpu, addr_mode);
		case 0x06:
			return dec(cpu, addr_mode);
		case 0x07:
			return inc(cpu, addr_mode);

		}
		break;

	case 0x03:
		/* No valid instructions */
		return EINVAL;
	}

	return ENOSYS;
}
