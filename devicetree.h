#pragma once

#include <stdint.h>

#include <module.h>
#include <clock.h>

struct cpu_specs {
	const char *clock;
};


int parse_dtb_prop(struct cpu_specs *dst, const struct dtb_property *prop,
                   find_phandle_t find_phandle, void *find_phandle_ctx);
